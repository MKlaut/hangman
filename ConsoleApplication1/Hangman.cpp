#include <iostream>
using namespace std;

int main()
{
	string wort = "";
	string dummy = "";
	char eingabe;
	int leben = 10;

	cout << "Bitte gewuenschtes Wort eingeben: ";
	cin >> wort;

	for (int i = 0; i < wort.length(); i++)
	{
		dummy.append("_");
	}
	cout << "Folgendes Wort wird gesucht" << dummy << endl;

	while (leben > 0)
	{
		cin >> eingabe;
		if (wort.find(eingabe) != string::npos)
		{
			for (int i = 0; i <= wort.length(); i++)
			{
				if (wort[i] == eingabe)
				{
					dummy[i] = eingabe;
				}
				else
				{
					continue;
				}
			}
			cout << dummy << endl;
		}
		else
		{
			leben--;
			cout << "Buchstabe nicht gefunden..." << endl;
			if (leben > 0)
			{
				cout << "Noch " << leben << " Versuche uebrig." << endl;
				cout << dummy << endl;
			}
			else
			{
				cout << "Game Over..." << endl;
				break;
			}
		}
		if (dummy == wort)
		{
			cout << "Gewonnen!";
			break;
		}
	}
}
